import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './unsplashAlbum.dart';
import './imageScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

Future<List<UnsplashAlbum>> fetchAlbum() async {
  final response = await http.get(Uri.parse(
      'https://api.unsplash.com/photos/?client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9'));
  List jsonresponse = jsonDecode(response.body);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return jsonresponse.map((ct) => UnsplashAlbum.fromJson(ct)).toList();
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<List<UnsplashAlbum>>(
          future: fetchAlbum(),
          builder: (context, snapshot) {
            print(snapshot.data);
            if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int i) {
                      return Card(
                        child: Stack(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) {
                                      return ImageScreen(
                                        imageUrl: snapshot.data[i].regular,
                                        id: snapshot.data[i].id,
                                      );
                                    },
                                  ),
                                );
                              },
                              child: Image.network(
                                snapshot.data[i].regular,
                                fit: BoxFit.fill,
                                width: double.infinity,
                              ),
                            ),
                            Positioned(
                              right: 0,
                              top: 10,
                              child: Container(
                                color: Colors.white,
                                width: 200,
                                child: Column(
                                  children: [
                                    Center(
                                      heightFactor: 1,
                                      child: Text(
                                          'Author: ${snapshot.data[i].username}' ??
                                              'default value'),
                                    ),
                                    Center(
                                        heightFactor: 2,
                                        child: Text(
                                            'Name: ${snapshot.data[i].name}' ??
                                                'default value')),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}


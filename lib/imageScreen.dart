import 'package:flutter/material.dart';

class ImageScreen extends StatelessWidget {
  final String imageUrl;
  final String id;

  const ImageScreen({Key key, this.imageUrl, this.id}) : super(key: key);

  static const routeScreen = '/imageScreen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: InkWell(
      child: Center(
        child: Hero(
          tag: id,
          child: Image.network(
            imageUrl,
            fit: BoxFit.fill,
            width: double.infinity,
          ),
        ),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    ));
  }
}

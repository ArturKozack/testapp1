class UnsplashAlbum {
  final String username;
  final String id;
  final String name;
  final String regular;

  UnsplashAlbum({
    this.username,
    this.id,
    this.name,
    this.regular,
  });

  factory UnsplashAlbum.fromJson(Map<String, dynamic> json) {
    return UnsplashAlbum(
      id: json["id"],
      username: json["user"]["username"],
      name: json["user"]["name"],
      regular: json["urls"]["regular"],
    );
  }
}

